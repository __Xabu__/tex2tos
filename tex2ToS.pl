#!/usr/bin/perl

###############################################################################
# Copyright (c) 2021 Alexander Bufe

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
###############################################################################

use strict;
use warnings;
use Getopt::Long;

my $abbr;
GetOptions("abbreviation"  => \$abbr)
or die("Error in command line arguments\n");


my %symbols;

while(<>){
	if( $abbr ){
		while( /\b([A-Z]+)\b/g ){
			push @{$symbols{$1}}, "$ARGV:$.";
		}
	}else{
		while( /(\$[^\$]+\$)/g ){
			push @{$symbols{$1}}, "$ARGV:$.";
		}
	}
} continue { 
    close ARGV if eof;
} 

print '\begin{longtable}[l]{ll}'."\n";
foreach my $symbol (sort keys %symbols){
	printf "\t%-25s &  \\\\ %% from %s \n", $symbol, join(" ", @{$symbols{$symbol}});
}
print '\end{longtable}'."\n";
