# tex2ToS

tex2ToS is a small perl script that simplifies the creation of a table of symbols (or abbreviations) for theses written in Latex. It parses the source files, extracts all equations written between dollar signs and outputs them as a longtable. As all variables should be introduced in text, this should give a good start for a table of symbols.

## Installation

Just download the script and make it executable. A Perl interpreter should be available on all GNU/Linux distributions.

## Usage

```bash
tex2ToS sections*.tex > tos.tex
```
You can then input the created file tos.tex in your latex project. Make sure you added
```latex
\usepackage{longtable}
```
to your preamble.

If you use the options --abbreviations or -a, a list of abbreviations, i.e. words that only consist of capital letters, will be created instead.

## Example
If used on the files partA.tex:
```latex
\section{Some Section}
\dots

The mass~$m$ can be converted into an energy~$e$ using Einstein's famous formula
\begin{equation}
    \label{eq:Einstein}
    e = m c^2,
\end{equation}
where $c$ is the speed of light.
```
and partB.tex:
```latex
\section{Some Other Section}

The volume of a sphere~$V_{\mathrm{sphere}}$ can be calculated from it's radius~$r$ according to
\begin{equation}
    V_{\mathrm{sphere}} = \frac 43 \pi r^3.
\end{equation}
The constant~$c$ is very important!
```
the output will be:
```latex
\begin{longtable}[l]{ll}
	$V_{\mathrm{sphere}}$     &  \\ % from partB.tex:3 
	$c$                       &  \\ % from partA.tex:9 partB.tex:7 
	$e$                       &  \\ % from partA.tex:4 
	$m$                       &  \\ % from partA.tex:4 
	$r$                       &  \\ % from partB.tex:3 
\end{longtable}
```
## License
[MIT](https://choosealicense.com/licenses/mit/)
